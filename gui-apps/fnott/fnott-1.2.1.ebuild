# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="Keyboard driven and lightweight Wayland notification daemon for wlroots-based compositors."
HOMEPAGE="https://codeberg.org/dnkl/fnott"
SRC_URI="https://codeberg.org/dnkl/fnott/archive/${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

COMMON_DEPEND="
	media-libs/fontconfig
	x11-libs/pixman
	media-libs/freetype
	media-libs/libpng
	dev-libs/wayland
	gui-libs/wlroots
	sys-apps/dbus
	media-libs/fcft
"

DEPEND="
	${COMMON_DEPEND}
	dev-libs/wayland-protocols
	dev-libs/tllist
	app-text/scdoc
"

src_install() {
	local DOCS=(LICENSE README.md)
	meson_src_install
	rm -r "${ED}/usr/share/doc/${PN}" || die
}



