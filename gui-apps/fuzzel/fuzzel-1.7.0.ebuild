# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="Application launcher similar to rofi's 'drun' mode"
HOMEPAGE="https://codeberg.org/dnkl/fuzzel"
SRC_URI="https://codeberg.org/dnkl/fuzzel/archive/${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}"/${PN}

LICENSE="MIT"
SLOT="0"
IUSE="cairo png svg"
KEYWORDS="~amd64"

IUSE="cairo png svg"

RDEPEND="
    dev-libs/wayland
    media-libs/fcft
    x11-libs/libxkbcommon
    cairo? ( x11-libs/cairo )
    png? ( media-libs/libpng )
    svg? ( gnome-base/librsvg )
"

DEPEND="${RDEPEND}
    dev-libs/tllist
	dev-libs/wayland-protocols
	app-text/scdoc
"

src_configure() {
    local emesonargs=(
        -Dwerror=false
        -Dpng-backend=$(usex png libpng none)
        -Dsvg-backend=$(usex svg librsvg none)
        $(meson_feature cairo enable-cairo)
    )
    meson_src_configure
}

src_install() {
    meson_src_install
    rm -r "${ED}/usr/share/doc/fuzzel" || die
    local -x DOCS=( LICENSE README.md CHANGELOG.md )
    einstalldocs
}



